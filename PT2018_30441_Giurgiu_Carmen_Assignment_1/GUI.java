package src;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

/**
 * @author Giurgiu Carmen
 * 
 * This is the class responsible for the graphical user interface. It is split into two separate tabs to avoid the over 
 * cluttering of the screen. 
 * The first tab entitled “Dynamic simulation” is the tab where users input the necessary information for the program to start
 * as well as where the simulation of the functioning itself is illustrated using java2D components.    
 * The second tab entitled ”Log” is where the dynamic evolution of the queues is displayed and saved.
 */
public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	static JLabel queueNrLabel = new JLabel("Input number of queues : ");
	static JTextField noOfQueuesField = new JTextField();
	static JLabel simulationTimeLabel = new JLabel("Input simulation time : ");
	static JTextField simulationTimeField = new JTextField();
	static JLabel serviceTimeLabel = new JLabel("Input values for service time : ");
	static JLabel minService = new JLabel("Minimum service :");
	static JTextField minServiceField = new JTextField();
	static JLabel maxService = new JLabel("Maximum service :");
	static JTextField maxServiceField = new JTextField();
	static JLabel arrivalTimeLabel = new JLabel("Input values for arrival time between customers :");
	static JLabel minArrivalTimeLabel = new JLabel("Minimum arrival time :");
	static JTextField minArrivalTimeField = new JTextField();
	static JLabel maxArrivalTimeLabel = new JLabel("Maximum arrival time :");
	static JTextField maxArrivalTimeField = new JTextField();
	static JButton buttonStart = new JButton(" Start Simulation ");
	
	static JLabel mainAverageLabel = new JLabel("Output data :");
	static JLabel peakTimeLabel = new JLabel();
	static JLabel averageService = new JLabel();
	static JLabel averageWait = new JLabel();
	static JLabel averageEmptyQueue = new JLabel();
	
	public static int minServingTime;
	public static int maxServingTime;
	public static int minArrivalTime;
	public static int maxArrivalTime;
	static JTabbedPane tabbedPane;
	static JPanel pane1;
	static JPanel pane2;
	private JTextArea display;
	private Clock clock;
	public int noOfQueues;
	private int serviceDuration;
	private int peakTime;
	private int peakValue;
	DrawArea drawArea;
	List<Queue> queues;
	List<Client> servedClients;


	public GUI() {
		queues = new ArrayList<Queue>();
		servedClients = new ArrayList<Client>();
		peakTime = 0;
		peakValue = 0;
		noOfQueues = 0;

		JPanel topPanel = new JPanel();
		setTitle("QueueSimulation");
		setSize(900, 600);
		setBackground(Color.gray);
		topPanel.setLayout(new BorderLayout());
		getContentPane().add(topPanel);
		
		createPage1();
		createPage2();
		
		tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Dynamic Simulation", pane1);
		tabbedPane.addTab("Log", pane2);
		topPanel.add(tabbedPane, BorderLayout.CENTER);

	}

	public Clock getClockTask() {
		return clock;
	}


	public void createPage1() {

		pane1 = new JPanel();
		pane1.setLayout(null);

		queueNrLabel.setBounds(575, 20, 150, 20);
		pane1.add(queueNrLabel);

		noOfQueuesField.setBounds(750, 20, 100, 20);
		pane1.add(noOfQueuesField);
		
		simulationTimeLabel.setBounds(575, 50, 150, 20);
		pane1.add(simulationTimeLabel);

		simulationTimeField.setBounds(750, 50, 100, 20);
		pane1.add(simulationTimeField);
		
		serviceTimeLabel.setBounds(575, 110, 200, 20);
		pane1.add(serviceTimeLabel);
		
		minService.setBounds(575, 140, 150, 20);
		pane1.add(minService);
		
		minServiceField.setBounds(750, 140, 100, 20);
		pane1.add(minServiceField);
		
		maxService.setBounds(575, 170, 150, 20);
		pane1.add(maxService);
		
		maxServiceField.setBounds(750, 170, 100, 20);
		pane1.add(maxServiceField);
		
		arrivalTimeLabel.setBounds(575, 230, 300, 20);
		pane1.add(arrivalTimeLabel);
		
		minArrivalTimeLabel.setBounds(575, 260, 200, 20);
		pane1.add(minArrivalTimeLabel);
		
		minArrivalTimeField.setBounds(750, 260, 100, 20);
		pane1.add(minArrivalTimeField);
		
		maxArrivalTimeLabel.setBounds(575, 290, 200, 20);
		pane1.add(maxArrivalTimeLabel);
		
		maxArrivalTimeField.setBounds(750, 290, 100, 20);
		pane1.add(maxArrivalTimeField);
		
		buttonStart.setBounds(615, 350, 200, 40);
		pane1.add(buttonStart);
		buttonStart.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				startSimulation();
				buttonStart.setEnabled(false);

			}
		});
		

		drawArea = new DrawArea(this);
		drawArea.setBounds(0, 0, 550, 550);
		pane1.add(drawArea);
	}
	
	public void createPage2() {
		

		pane2 = new JPanel();
		pane2.setLayout(null);

		display = new JTextArea(100, 100);
		display.setEditable(false); 
		JScrollPane scroll = new JScrollPane(display);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setBounds(0, 0, 550, 538);
		pane2.add(scroll);
		
		peakTimeLabel.setBounds(575, 20, 150, 20);
		pane2.add(peakTimeLabel);
		
		averageService.setBounds(575, 50, 300, 20);
		pane2.add(averageService);
		
		averageWait.setBounds(575, 80, 300, 20);
		pane2.add(averageWait);
		
		averageEmptyQueue.setBounds(575, 110, 300, 20);
		pane2.add(averageEmptyQueue);

	}


	public void logText(String text) {
		String fullText = display.getText();
		if (clock.getCurrTime() >= 0) {
			fullText += "Time " + clock.getCurrTime() + ": ";
		}
		fullText += text + "\n";
		display.setText(fullText);
	}


        @Override
	public void repaint() {
		drawArea.repaint();
	}

        /**
         * getShortestQueue() returns queue with shortest size
         * @return Queue
         */
	public Queue getShortestQueue() {
		int resultIndex = 0;
		int min = 1000;
		for (int i = 0; i < noOfQueues; i++) {
			Queue queue = queues.get(i);
			int size = queue.size();
			if (min > size) {
				min = size;
				resultIndex = i;
			}
		}
		return queues.get(resultIndex);
	}


	public void updatePeak() {
		int noOfCLients = 0;
		for (Queue queue : queues) {
			noOfCLients += queue.size();
		}
		if (noOfCLients > peakValue) {
			peakValue = noOfCLients;
			peakTime = clock.getCurrTime();
		}
	}

        /**
         * getPeakTime() returns second with most clients waiting on queues
         * @return 
         */
	public int getPeakTime() {
		return peakTime;
	}

        /**
         * getAverageServingTime() returns average serving time
         * @return double
         */
	public double getAverageServingTime() {
		double result = 0;
		if (!servedClients.isEmpty()) {
			for (Client client : servedClients) {
				result += client.getServingTime();
			}
			result = result / servedClients.size();
		}
		return result;
	}


        /**
         *  getAverageWaitingTime() returns average waiting time for served clients
         * @return double
         */
	public double getAverageWaitingTime() {
		double result = 0;
		if (!servedClients.isEmpty()) {
			for (Client client : servedClients) {
				result += client.getWaitingTime();
			}
			result = result / servedClients.size();
		}
		return result;
	}


        /**
         * getAverageEmptyQueueTime() retuns average time for empty queue(s)
         * @return double
         */
	public double getAverageEmptyQueueTime() {
		double result = 0;
		if (!queues.isEmpty()) {
			for (Queue queue : queues) {
				result += queue.getEmptyQueueCounter();
			}
			result = result / queues.size();
		}
		return result;
	}


	
	public static void main(String args[]) {
		
		GUI mainFrame = new GUI();
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}


	public void startSimulation() {
		String s1 = noOfQueuesField.getText();
		String s2 = simulationTimeField.getText();
		String s3 = minServiceField.getText();
		String s4 = maxServiceField.getText();
		String s5 = minArrivalTimeField.getText();
		String s6 = maxArrivalTimeField.getText();
		
		minServingTime = Integer.parseInt(s3);
		maxServingTime = Integer.parseInt(s4);
		minArrivalTime = Integer.parseInt(s5);
		maxArrivalTime = Integer.parseInt(s6);
		
		
		noOfQueues = Integer.parseInt(s1);
		serviceDuration = Integer.parseInt(s2);

		clock = new Clock(serviceDuration, this);
		Timer timer = new Timer("Clock");
		timer.schedule(clock, 0, 1000);
                
                         

	}
}
