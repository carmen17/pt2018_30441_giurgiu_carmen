package src;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Giurgiu Carmen
 * 
 * This is the class that is responsible for the simulation of the queues themselves. It implements the class Runnable 
 * and overrides its run method. It handles the clients once already in the queue (this is where we have the threads).  
 * 
 */
public class Queue implements Runnable {
	private List<Client> clients;
	private GUI gui;
	private int index;
	private int emptyQueueCounter;


	public Queue(GUI gui, int index) {
		super();
		this.gui = gui;
		this.index = index;
		clients = new ArrayList<Client>();
		emptyQueueCounter = 0;
	}


        /**
         * getEmptyQueueCounter() returns 1 if the queue is empty and 0 otherwise; used to compute average empty queue time
         * @return  int
         * @see GUI.getAverageServingTime()
         */
	public int getEmptyQueueCounter() {
		return emptyQueueCounter;
	}


        /**
         * getIndex() returns queue index int the list of queues
         * @return int
         * @see Clock.addClient()
         */
	public int getIndex() {
		return index;
	}


	@Override
	public void run() {

		int currTime = gui.getClockTask().getCurrTime();
		while (-2 != currTime) {
			Client currClient = null;
			if (!clients.isEmpty()) {
				currClient = clients.get(0);
			}
			if (null != currClient) {
				if (-1 == currClient.getStartOfServingTime()) {
					currClient.setStartOfServingTime(currTime);
					currClient.setWaitingTime(currTime - currClient.getArrivingTime() + currClient.getServingTime());
				}
				if (currTime - currClient.getStartOfServingTime() >= currClient.getServingTime()) {
					dequeue();
				}
			} else {
				emptyQueueCounter++;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			currTime = gui.getClockTask().getCurrTime();
		}
	}


        /**
         * enqueue() method is ised to add clients to the list of clients
         * @param client 
         */
	public void enqueue(Client client) {
		clients.add(client);
		gui.logText("Client " + client.getClientNumber() + " joined queue " + index);
	}


        /**
         * dequeue() method responsible for removing the client that is already served from the queue. The client is 
         * removed from the list of clients and the corresponding message is displayed in the log area of the GUI.
         */
	private synchronized void dequeue() {
		if (clients.size() > 0) {
			Client client = clients.get(0);
			gui.servedClients.add(client);
			clients.remove(0);
			gui.logText("Client " + client.getClientNumber() + " left queue " + index + " " + client.toString());
		}
	}

        /**
         * isEmpty() returns true is the list of clients is empty and false otherwise
         * @return 
         */
	public boolean isEmpty() {
		return clients.isEmpty();
	}

        
        /**
         * size() returns the size of the list of clients
         * @return int
         */
	public int size() {
		return clients.size();
	}

        /**
         * front() returns first client in the queue provided that the queue is not emoty
         * @return Client
         */
	public Client front() {
		Client result = null;
		if (!clients.isEmpty()) {
			result = new Client(clients.get(0));
		}
		return result;
	}
}
