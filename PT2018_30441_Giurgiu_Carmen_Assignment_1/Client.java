package src;

/**
 *
 * @author Giurgiu Carmen
 * 
 * The class is responsible for the modeling of the client in the application. It contains all information that the system 
 * needs to know about the client (arrival time, waiting time, service time, client number etc) and contains basic methods 
 * to return or set these values (getters and setters).
 * 
 */
public class Client {

	private int arrivingTime;
	public int servingTime;
	private int waitingTime;
	private int queueIndex;
	private int clientNumber;
	private int startOfServingTime;


	public Client(Client client) {			
		arrivingTime = client.arrivingTime;
		servingTime = client.servingTime;
		waitingTime = client.waitingTime;
		queueIndex = client.queueIndex;
                clientNumber = client.clientNumber;
		startOfServingTime = client.startOfServingTime;
	}


	public Client(int arrivingTime, int queueIndex, int clientNumber) {		
		this.clientNumber = clientNumber;
		this.arrivingTime = arrivingTime;
		this.queueIndex = queueIndex;		
		startOfServingTime = -1;
	}


	public int getArrivingTime() {
		return arrivingTime;
	}


	public int getQueueIndex() {
		return queueIndex;
	}


	public int getServingTime() {
		return servingTime;
	}


	public void setServingTime(int servingTime) {
		this.servingTime = servingTime;
	}


	public int getWaitingTime() {
		return waitingTime;
	}


	public void setWaitingTime(int waitingTime) {
		this.waitingTime = waitingTime;
	}


	public int getStartOfServingTime() {
		return startOfServingTime;
	}


	public void setStartOfServingTime(int startOfServingTime) {
		this.startOfServingTime = startOfServingTime;
	}

	
	public int getClientNumber() {
		return clientNumber;
	}	


	@Override
	public String toString() {
            return "[Arrival time : " + arrivingTime + ", service time : " + servingTime + ", waiting time : " + waitingTime + "]";
	}
}
