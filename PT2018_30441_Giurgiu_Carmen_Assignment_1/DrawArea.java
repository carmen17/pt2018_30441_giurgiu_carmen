package src;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 *
 * @author Giurgiu Carmen
 * 
 * This is the class that is responsible for the overall functionality of the application: it generates arrival times for 
 * clients, starts the threads, repaints the GUI, generates the time for the application and adds clients to the queues. 
 */
class DrawArea extends JPanel {

	GUI gui;
	private static final long serialVersionUID = 1L;


	public DrawArea(GUI gui) {
		super();
		this.gui = gui;
	}


        @Override
	public Dimension getPreferredSize() {
		return new Dimension(240, 50);
	}


        @Override
	protected void paintComponent(Graphics g) {
		g.setColor(Color.gray);
		g.fill3DRect(0, 0, 550, 550, false);
		g.setColor(Color.red);

		if ((gui != null) && (gui.noOfQueues > 0)) {
			for (int i = 0; i < gui.noOfQueues; i++) {
				g.fill3DRect(70 * i + 20, 20, 50, 50, true);
			}

			g.setColor(Color.orange);

			for (int i = 0; i < gui.noOfQueues; i++) {
				int people = gui.queues.get(i).size();
				for (int j = 0; j < people; j++) { // clients.size();
					g.fillOval(70 * i + 25, 60 * j + 80, 40, 40);

				}

			}
		}
	}
}