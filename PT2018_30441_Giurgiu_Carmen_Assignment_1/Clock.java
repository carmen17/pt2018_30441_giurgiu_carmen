package src;

import java.util.Random;
import java.util.TimerTask;


/**
 *
 * @author Giurgiu Carmen
 * 
 * This is the class that is responsible for the overall functionality of the application: it generates arrival times for 
 * clients, starts the threads, repaints the GUI, generates the time for the application and adds clients to the queues. 
 * 
 */
public class Clock extends TimerTask {
	private int currTime;
	private int totalTime;
	private int totalNumberOfClients;
	private GUI gui;
	public int[] arrTime;
	private boolean firstRun;
        
        public static int minimumArrivTime;
        public static int maximumArrivTime;


	public Clock(int totalTime, GUI gui) {
		
		totalNumberOfClients = 0;
		this.totalTime = totalTime;
		this.gui = gui;
		currTime = -1;
		firstRun = true;
	}


        /**
         * getCurrTime() returns current time
         * @return int
         */
	public int getCurrTime() {
		return currTime;
	}
        
        /**
         * getMinimumArrivTime() returns minimum interval of arriving time between customers
         * @return int
         */
        public int getMinimumArrivTime(){
            return minimumArrivTime;
        }
        
        /**
         * getMaximumArrivTime() returns maximum interval of arriving time between customers
         * @return int
         */
         public int getMaximumArrivTime(){
            return minimumArrivTime;
        }
         

	@Override
	public void run() {
		
		if (firstRun) {
			arrTime = new int[totalTime];
			firstRun = false;
			Random random = new Random();
			int time = 0;
			int min = GUI.minArrivalTime;
			int max = GUI.maxArrivalTime;
			while (time < totalTime) {
				time = time + min + random.nextInt(max-min+1);
				if (time < totalTime) {
					arrTime[time] = 1;
				}
			}
			for (int i = 0; i < gui.noOfQueues; i++) {
				Queue queue = new Queue(gui, i);
				gui.queues.add(queue);
				Thread thread = new Thread(queue);
				thread.start();
			}

		}
		currTime++;
		gui.repaint();
		if (currTime >= totalTime) {
			this.cancel();
			currTime = -2;
			logReport();                        
                        
		} else {
			addClient();
			gui.updatePeak();
		}
	}


	private void logReport() {
		GUI.peakTimeLabel.setText("Peak time: " + gui.getPeakTime());
		GUI.averageService.setText("Average serving time: " + gui.getAverageServingTime());
		GUI.averageWait.setText("Average waiting time: " + gui.getAverageWaitingTime());
		GUI.averageEmptyQueue.setText("Average empty queue time: " + gui.getAverageEmptyQueueTime());
	}


        /**
         * addClient() checks the array of arrival times. If on the position of the current time, we have a 1, that means that 
         * a new client needs to be added to one of the queues: the right queue is selected as the queue with the least clients 
         * in it, a new Client is created with a new arrival time( that is the current time ), an index of the queue it was 
         * added to,  and a number that is displayed as the client number in the log.
         * @see GUI.getShortestQueue()
         */
	private void addClient() {
		Random random = new Random();
		if (arrTime[currTime] == 1) {
			Queue queue = gui.getShortestQueue();
			Client client = new Client(currTime, queue.getIndex(),totalNumberOfClients++);
			client.setServingTime(client.servingTime = GUI.minServingTime + random.nextInt(GUI.maxServingTime -GUI.minServingTime + 1));
			queue.enqueue(client);
		}
	}
}
